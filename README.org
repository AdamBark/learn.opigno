* learn.opigno
** Installation
   This project relies on a running Traefik instance which can be found [[https://gitlab.com/AdamBark/edgerouter][here.]]
** Configuration
   Define environment variables by copying example.env to .env and changing the defaults. It may be advisable to set a server_name directive in opigno.conf by uncommenting and editing that line
** Running
   ~docker-compose --file docker-compose-traefik.yml up -d~
   
